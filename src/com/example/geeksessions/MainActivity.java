
package com.example.geeksessions;


import java.text.ParseException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class MainActivity extends Activity
{

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button btn= (Button)findViewById(R.id.testebtn);
		
		btn.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick( View v ) {
				Intent i = new Intent(MainActivity.this, NewEventActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				String message = "{'GEEKSESSION':{'start':'201303201745','end':'201303201900','where':'Porto','title':'Geek Session','description':'Android Development'}}";
				try{
					i.putExtra("smsdata", new GeekSession(message) );
				}catch( ParseException e ){
					Log.e("GEEKSESSIONS",e.getMessage(),e);
				}

				startActivity(i);
			}
		});
		
	}

	

	@Override
	public boolean onCreateOptionsMenu( Menu menu ) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
