/**
 * 
 */
package com.example.geeksessions;

import java.text.ParseException;
import java.util.regex.Pattern;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;


/**
 * @author RSousa
 *
 */
public class SMSReceiver extends BroadcastReceiver
{

	//FIXME: use a proper pattern (for a specific number)
	private static final Pattern filterPattern = Pattern.compile("(00|\\+351)9[1236]\\d{7}");

	
	@Override
	public void onReceive( Context context, Intent intent ) {

		Bundle extras= intent.getExtras();

		GeekSession message= processMessage(extras);
		
		if(message!=null){
			
			Intent i = new Intent(context,NewEventActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra("smsdata",message);
			context.startActivity(i);
			
		}
		
	}
	
	public GeekSession processMessage(Bundle bundle){

		GeekSession message = null;
		
		if (bundle!=null){
			
			// ---retrieve the received message here ---
			Object[] pdus= (Object[]) bundle.get("pdus");
			StringBuilder data = new StringBuilder();
			
			if(pdus!=null && pdus.length>0){
				
				for(Object pdu : pdus){
					
					SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdu);
					
					String sender= sms.getOriginatingAddress();
					String content = sms.getMessageBody();
					
					if(filterPattern.matcher(sender).matches()){
						data.append(content);
						
					}
					
				}
				
				try{
					if(data.length()>0){
						message = new GeekSession(data.toString());
						abortBroadcast();//do not propagate message to other services
					}
					
				}catch( ParseException e ){
					Log.i("GEEKSESSIONS", "not a GS message ("+e.getMessage()+")");
				}
			}
		}
		
		return message;
	}

}
